# File Processing T1 2017 #

### This is a homework git repository, feel free to peek or ask questions personally but plagiarism is discourages unless you're courages enough. ###

 ---
### Assignments ###
|#      |Assignments        |Due            | Status        | Branch |
| :---: |:----------------:|:------------:|:-------------:|:-------------:|
| A1    | Running Time Analysis  | 26 - SEPT - 17 | Completed     | master
| A2    | Running Time Analysis and Algorithms  | 03 - OCT - 17 | Completed     | master
| A3    | Divide and Conquer   | 10 - OCT - 17 | Completed   | master
| A4    | Graph   | 17 - OCT - 17 | Completed   | master
| A5    | Greedy   | 24 - OCT - 17 | Completed   | master
| A6    | Dynamic Programming   | 17 - NOV - 17 | In-Completed   | A6


### Exam ###
|#      |Assignments        |Due            | Status        | Branch |
| :---: |:----------------:|:------------:|:-------------:|:-------------:|
|       |                   |             |               |                |


#### Found a bug? Look for me in 1409
