import java.util.Scanner;

public class RepeatedString {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.next();
        long n = scanner.nextLong();

        System.out.println(solve(str, n));
    }

    private static long solve(String str, long n){
        long sum = 0;
        sum += countChar(str) * (n/str.length());
        sum += countChar(str, (int) (n%str.length()));
        return sum;
    }

    private static long countChar(String str, int length){
        long len = 0;
        for (int i = 0; i < length; i++){
            if (str.substring(i, i+1).equals("a"))
                len++;
        }
        return len;
    }

    private static long countChar(String str){
        return countChar(str, str.length());
    }
}
