import java.io.*;
import java.util.*;

public class MC {

    static class Pair<T extends Comparable<T>> implements Comparable<Pair<T>> {
        private T pairOne;
        private T pairTwo;

        public Pair(T pairOne, T pairTwo) {
            this.pairOne = pairOne;
            this.pairTwo = pairTwo;
        }

        public T getPairOne() {
            return pairOne;
        }

        public T getPairTwo() {
            return pairTwo;
        }

        @Override
        public int compareTo(Pair<T> o) {
            int n = this.pairOne.compareTo(o.pairOne);
            return n == 0 ? this.pairTwo.compareTo(o.pairTwo) : n;
        }

        @Override
        public String toString() {
            return String.format("[%s, %s]", pairOne.toString(), pairTwo.toString());
        }
    }
    static class FastScanner {
        private BufferedReader reader;
        private StringTokenizer token;

        public FastScanner(InputStream in) {
            reader = new BufferedReader(new InputStreamReader(in));
        }

        private String next() {

            String out = null;
            try {
                String st;
                if ((token == null || !token.hasMoreElements())){
                    if ((st = reader.readLine()) != null)
                        token = new StringTokenizer(st);
                    else
                        return null;

                }
                out = token.nextToken();
            } catch (IOException | NoSuchElementException e){}

            return out;
        }

        public String nextString(){
            return next();
        }

        public Integer nextInt(){
            return Integer.parseInt(next());
        }

        public Double nextDouble(){
            return Double.parseDouble(next());
        }

        public Long nextLong(){
            return Long.parseLong(next());
        }
    }


    public static void main(String[] args) throws Exception {
        FastScanner fs = new FastScanner(System.in);

        int n = fs.nextInt();
        for (int i = 0; i < n; i++){
            int M = fs.nextInt();
            fs.nextString();
            List<Pair<Integer>> pairs = new ArrayList<>();
            List<Pair<Integer>> answer = new ArrayList<>();

            while (true){
                int one = fs.nextInt(), two = fs.nextInt();

                if (one == 0 && two == 0) break;

                pairs.add(new Pair<>(one, two));
            }

            Collections.sort(pairs);

            int m = 0, c = 0;
            while (n < M) {
                int idx = -1;
                boolean done = false;
                while (c < pairs.size() && pairs.get(c).getPairOne() <= m)
                    if (pairs.get(c).pairTwo > m && (!done || pairs.get(c).getPairTwo() > pairs.get(idx).getPairTwo())) {
                        done = true;
                        idx = c++;
                    }
                    else c++;

                if (!done) break;
                else answer.add(pairs.get(idx));
                m = pairs.get(idx).getPairTwo();
            }
            if (m < M)
                System.out.print("0\n");
            else {
                System.out.printf("%d\n", answer.size());
                for (Pair re : answer) System.out.printf("%d %d\n", re.getPairOne(), re.getPairTwo());
            }
        }

    }
}
