import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.List;

public class MinimalCoverage {

    static class FastScanner {
        private BufferedReader reader;
        private StringTokenizer token;

        public FastScanner(InputStream in) {
            reader = new BufferedReader(new InputStreamReader(in));
        }

        private String next() {

            String out = null;
            try {
                String st;
                if ((token == null || !token.hasMoreElements())){
                    if ((st = reader.readLine()) != null)
                        token = new StringTokenizer(st);
                    else
                        return null;
                }
                out = token.nextToken();
            } catch (IOException | NoSuchElementException e){}

            return out;
        }

        public String nextString(){
            return next();
        }

        public Integer nextInt(){
            return Integer.parseInt(next());
        }

        public Double nextDouble(){
            return Double.parseDouble(next());
        }

        public Long nextLong(){
            return Long.parseLong(next());
        }
    }


    static class Pair implements Comparable<Pair> {
        int pairOne, pairTwo;

        Pair(int s, int e) {
            pairOne = s;
            pairTwo = e;
        }

        @Override
        public int compareTo(Pair that) {
            return pairOne == that.pairOne ? that.pairTwo - pairTwo : pairOne - that.pairOne;
        }
    }


    public static void main(String[] args) throws Exception {
        Scanner scan = new Scanner(System.in);

        int round = scan.nextInt();

        for (int i = 0; i < round; i++){
            int M = scan.nextInt();
            List<Pair> list = new ArrayList<>();

            while (true) {
                int p1 = scan.nextInt();
                int p2 = scan.nextInt();


                if (p1 == 0 && p2 == 0) break;

                list.add(new Pair(p1, p2));
            }
            Collections.sort(list);
            List<Pair> out = new ArrayList<>();

            int n = 0, c = 0;
            while (n < M) {
                int j = -1;
                boolean run = false;
                while (c < list.size() && list.get(c).pairOne <= n)
                    if (list.get(c).pairTwo > n && (!run || list.get(c).pairTwo > list.get(j).pairTwo)) {
                        run = true;
                        j = c++;
                    }
                    else
                        c++;


                if (!run) break;

                else
                    out.add(list.get(j));

                n = list.get(j).pairTwo;
            }

            if (n < M)
                System.out.print("0\n");
            else {
                System.out.printf("%d\n", out.size());
                for (Pair re : out) System.out.printf("%d %d\n", re.pairOne, re.pairTwo);
            }
        }
    }
}
