import java.util.*;
import java.io.*;

public class TheShortestPath {
    static class FastScanner {
        private BufferedReader reader;
        private StringTokenizer token;

        public FastScanner(InputStream in) {
            reader = new BufferedReader(new InputStreamReader(in));
        }

        private String next() {
            while (token == null || !token.hasMoreTokens() ) {
                //TODO add check for eof if necessary
                try {
                    token = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                }
            }
            return token.nextToken();
        }

        public String nextString(){
            return next();
        }

        public Integer nextInt(){
            return Integer.parseInt(next());
        }

        public Double nextDouble(){
            return Double.parseDouble(next());
        }

        public Long nextLong(){
            return Long.parseLong(next());
        }
    }

    public static void main(String[] args) {
        FastScanner fs = new FastScanner(System.in);
        int testCase = fs.nextInt();

        for (int i = 0; i < testCase; i++){
            int citiesCount = fs.nextInt();
            Map<String, Integer> cities = new HashMap<>(citiesCount);

            List<Node>[] graph = new ArrayList[citiesCount];

            for (int c = 0; c < citiesCount; c++){
                String name = fs.nextString();

                cities.put(name, c);

                int neighbours = fs.nextInt();

                List<Node> arr = new ArrayList(neighbours + 10);
                graph[c] = arr;
                for (int n = 0; n < neighbours; n++){
                    int nr = fs.nextInt()-1;
                    int cost = fs.nextInt();
                    graph[c].add(new Node(nr, cost));

                }
            }

            int paths = fs.nextInt();
            for (int p = 0; p < paths; p++){
                String name1 = fs.nextString();
                String name2 = fs.nextString();

                System.out.println(solve(graph, cities.get(name1), cities.get(name2), citiesCount));
            }
        }
    }

    static class Node implements Comparable<Node>{
        Integer v;
        Integer w;

        public Node(Integer v, Integer w) {
            this.v = v;
            this.w = w;
        }

        @Override
        public int compareTo(Node o) {
            return this.w.compareTo(o.w);
        }
    }


    static int solve(List<Node>[] edges,int s, int f, int n) {
        PriorityQueue<Node> pq = new PriorityQueue<>(n);
        int walkdist[] = new int[n];
        Set<Integer> visited = new HashSet<>(n);
        Arrays.fill(walkdist, Integer.MAX_VALUE);
        walkdist[s] = 0;
        for (int i = 0; i < n; i++){
            pq.add(new Node(i, walkdist[i]));
        }

        Node node;
        while (!pq.isEmpty() && (node = pq.poll()).v != f) {
            if (!visited.contains(node.v)){

                for (int v = 0; v < edges[node.v].size(); v++) {
                    Node next = edges[node.v].get(v);

                    if (!visited.contains(next.v)) {
                        int dist = walkdist[node.v] + next.w;
                        if (walkdist[next.v] > dist) {
                            pq.add(new Node(next.v, dist));
                            walkdist[next.v] = dist;
                        }
                    }
                }
                visited.add(node.v);
            }
        }

        return walkdist[f];
    }
}
