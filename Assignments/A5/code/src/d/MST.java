package d;

import java.io.*;
import java.util.*;

public class MST {
    static class FastScanner {
        private BufferedReader reader;
        private StringTokenizer token;

        public FastScanner(InputStream in) {
            reader = new BufferedReader(new InputStreamReader(in));
        }

        private String next() {

            String out = null;
            try {
                String st;
                if ((token == null || !token.hasMoreElements())){
                    if ((st = reader.readLine()) != null){
                        token = new StringTokenizer(st);
                    }
                    out = token.nextToken();
                }
            } catch (IOException | NoSuchElementException e){}

            return out;
        }

        public String nextString(){
            return next();
        }

        public Integer nextInt(){
            return Integer.parseInt(next());
        }

        public Double nextDouble(){
            return Double.parseDouble(next());
        }

        public Long nextLong(){
            return Long.parseLong(next());
        }
    }

    static class Pair<T extends Comparable<T>> implements Comparable<Pair<T>> {
        private T nr;
        private T cost;

        public Pair(T nr, T cost) {
            this.nr = nr;
            this.cost = cost;
        }

        public T getNr() {
            return nr;
        }

        public T getCost() {
            return cost;
        }

        @Override
        public int compareTo(Pair<T> o) {
            int n = this.nr.compareTo(o.nr);
            return n == 0 ? this.cost.compareTo(o.cost) : n;
        }

        @Override
        public String toString() {
            return String.format("[%s, %s]", nr.toString(), cost.toString());
        }
    }
    static class City {
        private String name;
        private int uid;
        private List<Pair<Integer>> costs;

        public City(String name, int uid) {
            this.name = name;
            this.uid = uid;
            costs = new ArrayList<>();
        }

        public String getName() {
            return name;
        }

        public int getUid() {
            return uid;
        }

        public List<Pair<Integer>> getCosts() {
            return costs;
        }

        @Override
        public String toString() {
            return "Node{" +
                    "name='" + name + '\'' +
                    ", uid=" + uid +
                    ", costs=" + costs +
                    '}';
        }
    }

    static class WeightedGraph {
//        Map
    }



    public static void main(String[] args) {

        FastScanner fs = new FastScanner(System.in);
        int n = fs.nextInt();


        for (int i = 0; i < n; i++){
            int cities = fs.nextInt();

            for (int j = 0; j < cities; j++){
                String name = fs.nextString();

            }

        }
    }
}
