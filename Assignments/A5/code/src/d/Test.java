package d;

import java.io.*;
import java.util.*;

public class Test {
    static class Node implements Comparable<Node> {
        //node #
        private Integer n;
        // path
        private Integer dist;

        public Node(int n, int d) {
            this.n = n;
            dist = d;
        }

        @Override
        public int compareTo(Node o) {
            return this.dist.compareTo(o.dist);
        }

        public Integer getDist() {
            return dist;
        }

        public Integer getN() {
            return n;
        }

        public void setDist(Integer dist) {
            this.dist = dist;
        }

        public void setN(Integer n) {
            this.n = n;
        }
    }

    static class WGraph {

        ArrayList<Node> edges[];
        int n;

        public WGraph(int n) {
            this.n = n;
            edges = new ArrayList[n];
            for (int i = 0; i < n; i++) {
                edges[i] = new ArrayList<>();
            }
        }

        public void addEdges(int u, int v, int c) {
            edges[u].add(new Node(v, c));
        }

        int dijkstra(int s, int f) {
            PriorityQueue<Node> pq = new PriorityQueue<>(n);
            //min distance from s
            int dist[] = new int[n];
            Set<Integer> visited = new HashSet<>(n);
            for (int i = 0; i < n; i++) {
                if (i == s) {
                    dist[i] = 0;
                } else {
                    dist[i] = Integer.MAX_VALUE;
                }
                Node nn = new Node(i, dist[i]);
                pq.add(nn);
            }

            while (!pq.isEmpty()) {
                Node nu = pq.poll();
                int u = nu.getN();
                int d = nu.getDist();
                if (u == f) {
                    return dist[u];
                }
                if (!visited.contains(u)){
                    for (int v = 0; v < edges[u].size(); v++) {
                        Node t = edges[u].get(v);
                        int temp = t.n;
                        if (!visited.contains(temp)) {
                            int nd = dist[u] + t.dist;
                            if (dist[temp] > nd) {
                                pq.add(new Node(t.n, nd));
                                dist[temp] = nd;
                            }
                        }
                    }
                    visited.add(u);
                }
            }
            return dist[f];
        }
    }

    static class Reader {
        static BufferedReader reader;
        static StringTokenizer tokenizer;
        static void init(InputStream input) {
            reader = new BufferedReader(
                    new InputStreamReader(input) );
            tokenizer = new StringTokenizer("");
        }
        static String next() throws IOException {
            while ( ! tokenizer.hasMoreTokens() ) {
                //TODO add check for eof if necessary
                tokenizer = new StringTokenizer(
                        reader.readLine() );
            }
            return tokenizer.nextToken();
        }
        static int nextInt() throws IOException {
            return Integer.parseInt( next() );
        }
    }
    public static void main(String[] args) throws IOException {
        Reader.init(System.in);
        // # test cases
        int tc = Reader.nextInt();
        while(tc-- > 0){
            // #cities
            int n = Reader.nextInt();
            ArrayList<String> cities = new ArrayList<>(n);
            WGraph g = new WGraph(n);
            int c_count = 0;
            for (int i = 0; i < n; i++) {
                String name = Reader.next();
                cities.add(c_count++, name);
                int p = Reader.nextInt();
                for (int j = 0; j < p; j++) {
                    int nr = Reader.nextInt();
                    int cost = Reader.nextInt();
                    g.addEdges(i, nr - 1, cost);
                }
            }

            int r = Reader.nextInt();
            while(r-- > 0) {
                String s = Reader.next();
                String f = Reader.next();
                int inS = cities.indexOf(s);
                int inF = cities.indexOf(f);
                System.out.println(g.dijkstra(inS, inF));
            }
        }
        return;
    }
}
