package d;

import java.io.*;
import java.util.*;

public class TheShortestPath {

    static class WeightedEdge<T extends Comparable<T>, Q extends Number & Comparable<Q>> implements Comparator<Q>, Comparable<WeightedEdge<T, Q>> {
        private T nodeA;
        private T nodeB;
        private Q weight;

        public WeightedEdge(T nodeA, T nodeB, Q weight) {
            this.nodeA = nodeA;
            this.nodeB = nodeB;
            this.weight = weight;
        }

        public T getNodeA() {
            return nodeA;
        }

        public T getNodeB() {
            return nodeB;
        }

        public Q getWeight() {
            return weight;
        }

        @Override
        public int compare(Q o1, Q o2) {
            return o1.compareTo(o2);
        }

        @Override
        public int compareTo(WeightedEdge<T, Q> o) {
            return this.weight.compareTo(o.weight);
        }

        @Override
        public String toString() {
            return "[" + this.nodeA +" -> " + this.nodeB+ ", weight=" + this.weight + "]";
        }
    }


    static class WeightedGraph<T extends Comparable<T>, Q extends Number & Comparable<Q>> {
        private Map<T, Set<WeightedEdge<T, Q>>> graph;
        private Set<WeightedEdge<T,Q>> uniqueEdges;
        private Integer edges;
        private WeightedEdge<T, Q> smallestNode;

        public WeightedGraph() {
            this.graph = new HashMap<>();
            this.edges = 0;
            this.uniqueEdges = new HashSet<>();
        }

        public void addNode(T u, T v, Q w){
            edges++;
            WeightedEdge<T, Q> node = new WeightedEdge<>(u, v, w);
            this.uniqueEdges.add(node);

            if (! this.graph.containsKey(u))
                this.graph.put(u, new HashSet<>());

            this.graph.get(u).add(node);

            if (smallestNode == null || w.compareTo(smallestNode.getWeight()) == -1)
                smallestNode = node;

        }

        public void union(T u, T v){

        }

        public Set<WeightedEdge<T,Q>> getAllEdges(){
            return uniqueEdges;
        }

        public boolean containVertex(T u){
            return graph.containsKey(u);
        }

        public Set<WeightedEdge<T, Q>> vertexOf(T u){
            return graph.get(u);
        }

        public Integer size(){
            return uniqueEdges.size();
        }

        public Integer edge(){
            return edges;
        }

        public WeightedEdge<T, Q> getSmallestNode() {
            return smallestNode;
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("WeightedGraph {").append("\n");
            Iterator<T> itr = graph.keySet().iterator();
            while (itr.hasNext()) {
                T next = itr.next();
                builder.append("\t").append(next).append(" -> ").append(this.graph.get(next)).append("\n");
            }

            builder.append("}");

            return builder.toString();
        }
    }
    static class FastScanner {
        private BufferedReader reader;
        private StringTokenizer token;

        public FastScanner(InputStream in) {
            reader = new BufferedReader(new InputStreamReader(in));
        }

        private String next() {
            while (token == null || !token.hasMoreTokens() ) {
                //TODO add check for eof if necessary
                try {
                    token = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                }
            }
            return token.nextToken();
        }

        public String nextString(){
            return next();
        }

        public Integer nextInt(){
            return Integer.parseInt(next());
        }

        public Double nextDouble(){
            return Double.parseDouble(next());
        }

        public Long nextLong(){
            return Long.parseLong(next());
        }
    }

    public static void main(String[] args) {
        FastScanner fs = new FastScanner(System.in);
        int testCase = fs.nextInt();

        for (int i = 0; i < testCase; i++){
            int citiesCount = fs.nextInt();

//            Node[] cities = new Node[citiesCount];
            Map<String, Integer> cities = new HashMap<>(citiesCount);
            WeightedGraph<Integer, Integer> graph = new WeightedGraph<>();

            for (int c = 0; c < citiesCount; c++){
                String name = fs.nextString();

                cities.put(name, c);

                int neighbours = fs.nextInt();
                for (int n = 0; n < neighbours; n++){
                    int nr = fs.nextInt();
                    int cost = fs.nextInt();
                    graph.addNode(c, nr-1, cost);
                }
            }

            int paths = fs.nextInt();
            for (int p = 0; p < paths; p++){
                String name1 = fs.nextString();
                String name2 = fs.nextString();

                System.out.println(solve(graph, new Node(cities.get(name1), cities.get(name2), 0, 0), cities.get(name2)));
            }
        }
    }

    static class Node implements Comparable<Node>{
        Integer start;
        Integer end;
        Integer sum;
        Integer cost;

        public Node(int start, int end, int cost, int sum) {
            this.start = start;
            this.end = end;
            this.cost = cost;
            this.sum = sum;
        }

        @Override
        public int compareTo(Node o) {
            return this.cost.compareTo(o.cost);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Node node = (Node) o;

            if (!start.equals(node.start)) return false;
            if (!end.equals(node.end)) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = start.hashCode();
            result = 31 * result + end.hashCode();
            return result;
        }

        @Override
        public String toString() {
            return "Node{" +
                    "start=" + start +
                    ", end=" + end +
                    ", sum=" + sum +
                    ", cost=" + cost +
                    '}';
        }
    }

    private static int solve(WeightedGraph<Integer, Integer> graph, Node node, Integer end){
        PriorityQueue<Node> pq = new PriorityQueue<>((int) Math.pow(graph.size(), 2) > Integer.MAX_VALUE ? Integer.MAX_VALUE : (int) Math.pow(graph.size(), 2));
        Set<Node> visited = new HashSet<>(graph.size());

        pq.add(node);

        while (!pq.isEmpty()){
            Node current = pq.poll();
            System.out.println(current);

            if (current.start == end){
                return current.cost;
            }

            if (!visited.contains(current)){
                if (graph.containVertex(current.start)){
                    for (WeightedEdge<Integer, Integer> n : graph.vertexOf(current.start)){
                        if (!visited.contains(n.nodeB)){
                            pq.add(new Node(n.nodeA ,n.nodeB, n.weight,current.sum + n.weight));
                        }
                    }
                }
                visited.add(current);
            }
        }

        return -1;
    }
}
