import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

public class CCP {
    static class FastScanner {
        private BufferedReader reader;
        private StringTokenizer token;

        public FastScanner(InputStream in){
            reader = new BufferedReader(new InputStreamReader(in));
        }

        private String next(){
            if (token == null || !token.hasMoreElements()) try {
                token = new StringTokenizer(reader.readLine());
            } catch (IOException e) {
//                    e.printStackTrace();
            }
            return token.nextToken();
        }

        public int nextInt(){
            return Integer.parseInt(next());
        }

        public double nextDouble(){
            return Double.parseDouble(next());
        }
    }
    public static void main(String[] args) {
        FastScanner scanner = new FastScanner(System.in);
        int n;
        while ((n = scanner.nextInt()) != 0){

            Point2D[] pointsX = new Point2D[n];
            Point2D[] pointsY = new Point2D[n];

            for (int i = 0; i < n; i++) {
                double x = scanner.nextDouble();
                double y = scanner.nextDouble();

                pointsX[i] = new Point2D.Double(x, y);
                pointsY[i] = new Point2D.Double(x, y);
            }

            if (n == 1){
                System.out.println("INFINITY");
            } else {
                double result = solve(pointsX, pointsY);

                if (result > 10000){
                    System.out.println("INFINITY");
                }else {
                    System.out.println(String.format("%.4f", result));
                }
            }
        }
    }
    private static int  size(int lo, int hi){
        return hi - lo +1;
    }

    private static double solve(Point2D[] pointX, Point2D[] pointY){
        Arrays.sort(pointX, Comparator.comparingDouble(Point2D::getX));
        Arrays.sort(pointY, Comparator.comparingDouble(Point2D::getY));
        return closestPoint(pointX, pointY,0, pointX.length);
    }

    private static double closestPoint(Point2D[] pointX, int lo, int hi){
        if (size(lo, hi) == 2)
            return pointX[0].distance(pointX[1]);
        else {
            double min = Double.MAX_VALUE;
            for (int i = lo; i < hi - 1; i++)
                for (int j = i + 1; j < hi; j++){
                    double dist = pointX[i].distance(pointX[j]);
                    if ( dist < min)
                    min = dist;
                }

            return min;
        }
    }

    private static double closestPoint(Point2D[] pointX, Point2D[] pointY, int lo, int hi){
        if (size(lo, hi)<= 3)
            return closestPoint(pointX, lo, hi);


        int mid = (lo + hi)/2;
        double left = closestPoint(pointX, pointY, lo, mid-1);
        double right = closestPoint(pointX, pointY, mid, hi-1);

        double delta = Math.min(left, right);
        double min = delta;

        Point2D midPoint = pointX[mid];

        for (int i = lo; i < hi - 1; i++){
            for (int j = i + 1; j < hi && (pointY[j].getY() - pointY[i].getY()) < delta; j++){
                min = Math.min(min, pointY[j].distance(pointY[i]));
            }
        }

        return min;

    }
}
