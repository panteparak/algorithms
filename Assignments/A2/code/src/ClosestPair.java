import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Comparator;
import java.util.StringTokenizer;

public class ClosestPair {
    static class FastScanner {
        private BufferedReader reader;
        private StringTokenizer token;

        public FastScanner(InputStream in){
            reader = new BufferedReader(new InputStreamReader(in));
        }

        private String next(){
            if (token == null || !token.hasMoreElements()) try {
                token = new StringTokenizer(reader.readLine());
            } catch (IOException e) {
//                    e.printStackTrace();
            }
            return token.nextToken();
        }

        public int nextInt(){
            return Integer.parseInt(next());
        }

        public double nextDouble(){
            return Double.parseDouble(next());
        }
    }

    static class Pair<T extends Point2D> {
        private T pointOne;
        private T pointTwo;
        private double distance;

        public Pair(T pointOne, T pointTwo) {
            this.pointOne = pointOne;
            this.pointTwo = pointTwo;
            this.distance = pointOne.distance(pointTwo);
        }

        public T getPointOne() {
            return pointOne;
        }

        public T getPointTwo() {
            return pointTwo;
        }

        public Pair<T> minimumPair(Pair<T> thatPair){
            return this.distance > thatPair.distance ? thatPair : this;
        }

        @Override
        public String toString() {
            return String.format("%.2f %.2f %.2f %.2f", pointOne.getX(), pointOne.getY(), pointTwo.getX(), pointTwo.getY());
        }
    }
    public static void main(String[] args) {
        FastScanner scanner = new FastScanner(System.in);
        int n;
        while ((n = scanner.nextInt()) != 0){

            Point2D[] pointsX = new Point2D[n];
            Point2D[] pointsY = new Point2D[n];

            for (int i = 0; i < n; i++) {
                double x = scanner.nextDouble();
                double y = scanner.nextDouble();

                pointsX[i] = new Point2D.Double(x, y);
                pointsY[i] = new Point2D.Double(x, y);
            }

//            if (n == 1){
//                System.out.println("INFINITY");
//            } else {
//                double result = solve(pointsX, pointsY).distance;
//
//                if (result > 10000){
//                    System.out.println("INFINITY");
//                }else {
//                    System.out.println(String.format("%.4f", result));
//                }
//            }

            System.out.println(solve(pointsX, pointsY));


        }
    }
    private static int size(int lo, int hi){
        return hi - lo +1;
    }

    private static Pair<Point2D> solve(Point2D[] pointX, Point2D[] pointY){
        Arrays.sort(pointX, Comparator.comparingDouble(Point2D::getX));
        Arrays.sort(pointY, Comparator.comparingDouble(Point2D::getY));
        return closestPoint(pointX, pointY,0, pointX.length);
    }

    private static Pair<Point2D> closestPoint(Point2D[] pointX, int lo, int hi){
        if (size(lo, hi) == 2)
            return new Pair<>(pointX[0], pointX[1]);
        else {
            Pair<Point2D> min = null;
            for (int i = lo; i < hi - 1; i++)
                for (int j = i + 1; j < hi; j++){
                    double dist = pointX[i].distance(pointX[j]);
                    if (min == null || min.distance > dist){
                        min = new Pair<>(pointX[i], pointX[j]);
                    }
                }

            return min;
        }
    }

    private static Pair<Point2D> closestPoint(Point2D[] pointX, Point2D[] pointY, int lo, int hi){
        if (size(lo, hi)<= 3)
            return closestPoint(pointX, lo, hi);

        int mid = (lo + hi)/2;
        Pair<Point2D> left = closestPoint(pointX, pointY, lo, mid-1);
        Pair<Point2D> right = closestPoint(pointX, pointY, mid, hi-1);

        Pair<Point2D> delta;
        Pair<Point2D> min;

        if (left.distance > right.distance){
            min = new Pair<>(right.getPointOne(), right.getPointTwo());
            delta = new Pair<>(right.getPointOne(), right.getPointTwo());
        }else {
            min = new Pair<>(left.getPointOne(), left.getPointTwo());
            delta = new Pair<>(left.getPointOne(), left.getPointTwo());
        }

        for (int i = lo; i < hi - 1; i++){
            for (int j = i + 1; j < hi && (pointY[j].getY() - pointY[i].getY()) < delta.distance; j++){
                if (min.distance > pointY[j].distance(pointY[i])){
                    min = new Pair<>(pointY[i], pointY[j]);
                }
            }
        }
        return min;
    }
}
