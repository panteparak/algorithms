package old;

import java.util.Arrays;

public class DuplicateMergeSort {

public Integer[] mergeSort(Integer[] input) {
    if (input.length <= 1)
        return input;

    int mid = input.length / 2;

    Integer[] left = Arrays.copyOfRange(input, 0, mid);
    Integer[] right = Arrays.copyOfRange(input, mid, input.length);

    return merge(mergeSort(left), mergeSort(right));
}

private Integer[] merge(Integer[] left, Integer[] right) {
    Integer[] out = new Integer[left.length + right.length];
    int i = 0, j = 0, k = 0;

    while(i < left.length && j < right.length){
        if(left[i] < right[j]){
            out[k] = left[i];
            i++;
        } else if (left[i] > right[j]){
            out[k] = right[j];
            j++;
        } else {
            out[k] = left[i];
            i++;
            j++;
        }
        k++;
    }

    if(j >= right.length){
        while(i < left.length){
            out[k] = left[i];
            i++;
            k++;
        }
    } else {
        while(j < right.length){
            out[k] = right[j];
            j++;
            k++;
        }
    }

    return out;
}

    public static void main(String[] args) {
        DuplicateMergeSort d = new DuplicateMergeSort();
        System.out.println(Arrays.toString(d.mergeSort(new Integer[]{1, 1, 10, 5, 9, 1})));
    }
}