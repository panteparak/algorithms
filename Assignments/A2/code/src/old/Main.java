package old;

import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Comparator;
import java.util.StringTokenizer;

public class Main {
    static class FastScanner {
        private BufferedReader reader;
        private StringTokenizer token;

        public FastScanner(InputStream in){
            reader = new BufferedReader(new InputStreamReader(in));
        }

        private String next(){
            if (token == null || !token.hasMoreElements()) try {
                    token = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
//                    e.printStackTrace();
                }
            return token.nextToken();
        }

        public int nextInt(){
            return Integer.parseInt(next());
        }

        public double nextDouble(){
            return Double.parseDouble(next());
        }
    }

    static class Pair<T extends Point2D>{
        private T pairOne;
        private T pairTwo;
        private double distance = 0.0;

        public Pair(T pairOne, T pairTwo) {
            this.pairOne = pairOne;
            this.pairTwo = pairTwo;
            this.distance = pairOne.distance(pairTwo);
        }

        public T getPairOne() {
            return pairOne;
        }

        public T getPairTwo() {
            return pairTwo;
        }

        public double getDistance() {
            return distance;
        }

        public Pair<T> compareDistance(Pair<T> that){
            return this.distance > that.distance ? that : this;
        }

        @Override
        public String toString() {
            return String.format("%.4f %.4f %.4f %.4f", pairOne.getX(), pairOne.getY(), pairTwo.getX(), pairTwo.getY());
        }
    }

    public static Pair<Point2D> solve(Point2D[] array){
        Point2D[] x = Arrays.copyOf(array, array.length);
        Point2D[] y = Arrays.copyOf(array, array.length);
        Arrays.sort(x, Comparator.comparingDouble(Point2D::getX));
        Arrays.sort(y, Comparator.comparingDouble(Point2D::getY));
        return closestPair(x, y);
    }

    public static Pair<Point2D> closestPair(Point2D[] points){
        if (points.length == 2) return new Pair<>(points[0], points[1]);
        else {
            Pair<Point2D> possiblePair = null;
            for (int i = 0; i < points.length - 1; i++){
                for (int j = i+1; j < points.length; j++){
                    Point2D pointOne = points[i];
                    Point2D pointTwo = points[j];
                    if (possiblePair == null || pointOne.distance(pointTwo) < possiblePair.distance)
                        possiblePair = new Pair<>(pointOne, pointTwo);
                }
            }
            return possiblePair;
        }
    }

    public static Pair<Point2D> closestPair(Point2D[] sortedX, Point2D[] sortedY){

        if (sortedX.length < 4)
            return closestPair(sortedX);

//        int mid = sortedX.length/2;
        int mid = sortedX.length - (sortedX.length/2);

        Pair<Point2D> left = closestPair(Arrays.copyOfRange(sortedX, 0, mid), sortedY);
        Pair<Point2D> right = closestPair(Arrays.copyOfRange(sortedX, mid, sortedX.length), sortedY);
        Pair<Point2D> delta = left.compareDistance(right);

        for (int i = 0; i < sortedY.length - 1; i++){
            for (int j = i + 1; j < sortedY.length && sortedY[i].distance(sortedY[j]) < delta.distance; j++){
                delta = new Pair<>(sortedY[i], sortedY[j]);
            }
        }

        return delta;
    }

    public static void main(String[] args) {
        FastScanner scanner = new FastScanner(System.in);

        int n;
        while ((n = scanner.nextInt()) != 0){
            Point2D[] points = new Point2D[n];

            for (int i = 0; i < n; i++){
                points[i] = new Point2D.Double(scanner.nextDouble(), scanner.nextDouble());
            }

            if (points.length < 2){
                System.out.println("INFINITY");
            }else {
                double result = solve(points).distance;

                if (result < 10000) System.out.printf("%.4f\n", result);
                else System.out.println("INFINITY");
            }
        }
    }
}
