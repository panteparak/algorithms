package old;

public class DQ {
    public static void main(String[] args) {
        Integer[] array = {1,2,3,7,3,2,1};
        System.out.println(bs(array, 0, array.length));
    }

    public static Integer bs(Integer[] array, int lo, int hi){
        if (hi - lo + 1 == 1) return array[0];
        else {
            int mid = (lo + hi) / 2;

            if ((mid + 1 < array.length) && (mid - 1 >= 0)) {
                if (array[mid + 1] > array[mid])
                    return bs(array, mid, hi);
//                else if (array[mid - 1] > array[mid])
//                    return bs(array, lo, mid);
                else
                    return bs(array, lo, mid);

            }else return null;
        }
    }
}
