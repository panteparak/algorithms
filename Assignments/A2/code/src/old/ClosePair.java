package old;

import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

public class ClosePair {
    public static class Point {
        public final double x;
        public final double y;

        public Point(double x, double y) {
            this.x = x;
            this.y = y;
        }

        public String toString() {
            return x + " " + y;
        }
    }

    public static class Pair {
        public Point point1 = null;
        public Point point2 = null;
        public double distance = 0.0;

        public Pair(Point point1, Point point2) {
            this.point1 = point1;
            this.point2 = point2;
            calcDistance();
        }

        public void update(Point point1, Point point2, double distance) {
            this.point1 = point1;
            this.point2 = point2;
            this.distance = distance;
        }

        public void calcDistance() {
            this.distance = distance(point1, point2);
        }

        public String toString() {
            return point1 + " " + point2;
        }
    }

    public static double distance(Point p1, Point p2) {
        double xdist = p2.x - p1.x;
        double ydist = p2.y - p1.y;
        return Math.hypot(xdist, ydist);
    }

    public static Pair bruteForce(List<? extends Point> points) {
        int numPoints = points.size();
        if (numPoints < 2)
            return null;
        Pair pair = new Pair(points.get(0), points.get(1));
        if (numPoints > 2) {
            for (int i = 0; i < numPoints - 1; i++) {
                Point point1 = points.get(i);
                for (int j = i + 1; j < numPoints; j++) {
                    Point point2 = points.get(j);
                    double distance = distance(point1, point2);
                    if (distance < pair.distance)
                        pair.update(point1, point2, distance);
                }
            }
        }
        return pair;
    }

    public static void sortByX(List<? extends Point> points) {
        Collections.sort(points, (x1, x2) -> Double.compare(x1.x, x2.x));
    }

    public static void sortByY(List<? extends Point> points) {
        Collections.sort(points, new Comparator<Point>() {
                    public int compare(Point point1, Point point2) {
                        if (point1.y < point2.y)
                            return -1;
                        if (point1.y > point2.y)
                            return 1;
                        return 0;
                    }
                }
        );
    }

    public static Pair divideAndConquer(List<? extends Point> points) {
        List<Point> pointsSortedByX = new ArrayList<Point>(points);
        sortByX(pointsSortedByX);
        List<Point> pointsSortedByY = new ArrayList<Point>(points);
        sortByY(pointsSortedByY);
        return divideAndConquer(pointsSortedByX, pointsSortedByY);
    }

    private static Pair divideAndConquer(List<? extends Point> pointsSortedByX, List<? extends Point> pointsSortedByY) {
        int numPoints = pointsSortedByX.size();
        if (numPoints <= 3)
            return bruteForce(pointsSortedByX);

        int dividingIndex = numPoints >>> 1; // Half length

        // Divide list into 2
        List<? extends Point> leftOfCenter = pointsSortedByX.subList(0, dividingIndex);
//        System.out.println(leftOfCenter);
        List<? extends Point> rightOfCenter = pointsSortedByX.subList(dividingIndex, numPoints);

        List<Point> tempList = new ArrayList<>(leftOfCenter);
        sortByY(tempList);
        Pair closestPair = divideAndConquer(leftOfCenter, tempList);

        tempList.clear();
        tempList.addAll(rightOfCenter);
        sortByY(tempList);
        Pair closestPairRight = divideAndConquer(rightOfCenter, tempList);

        if (closestPairRight.distance < closestPair.distance)
            closestPair = closestPairRight;

        tempList.clear();
        double shortestDistance = closestPair.distance;
        double centerX = rightOfCenter.get(0).x;
        for (Point point : pointsSortedByY)
            if (Math.abs(centerX - point.x) < shortestDistance)
                tempList.add(point);

        for (int i = 0; i < tempList.size() - 1; i++) {
            Point point1 = tempList.get(i);
            for (int j = i + 1; j < tempList.size(); j++) {
                Point point2 = tempList.get(j);
                if ((point2.y - point1.y) >= shortestDistance)
                    break;
                double distance = distance(point1, point2);
                if (distance < closestPair.distance) {
                    closestPair.update(point1, point2, distance);
                    shortestDistance = distance;
                }
            }
        }
        return closestPair;
    }

    static class FastScanner {
        private BufferedReader reader;
        private StringTokenizer tokenizer;

        public FastScanner(InputStream in) {
            this.reader = new BufferedReader(new InputStreamReader(in));
        }

        private String next(){
            if (tokenizer == null || !tokenizer.hasMoreElements()) try {
                tokenizer = new StringTokenizer(reader.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return tokenizer.nextToken();
        }

        public String nextString() {
            return next();
        }

        public double nextDouble(){
            return Double.parseDouble(next());
        }

        public int nextInt(){
            return Integer.parseInt(next());
        }

        public long nextLong(){
            return Long.parseLong(next());
        }

        public void close() {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        int numPoints = (args.length == 0) ? 100000 : Integer.parseInt(args[0]);
        List<Point> points = new ArrayList<>();
        Random r = new Random();
        for (int i = 0; i < numPoints; i++)
            points.add(new Point(r.nextDouble(), r.nextDouble()));
        System.out.println("Generated " + numPoints + " random points");
        long startTime = System.currentTimeMillis();
        Pair bruteForceClosestPair = bruteForce(points);
        long elapsedTime = System.currentTimeMillis() - startTime;
        System.out.println("Brute force (" + elapsedTime + " ms): " + bruteForceClosestPair);
        startTime = System.currentTimeMillis();
        Pair dqClosestPair = divideAndConquer(points);
        elapsedTime = System.currentTimeMillis() - startTime;
        System.out.println("Divide and conquer (" + elapsedTime + " ms): " + dqClosestPair);

        if (bruteForceClosestPair.distance != dqClosestPair.distance)
            System.out.println("MISMATCH");

//        FastScanner scanner = new FastScanner(System.in);
//        int n;
//        while ((n = scanner.nextInt()) != 0) {
//            List<Point> points = new ArrayList<>(n);
//
//            for (int i = 0; i < n; i++) {
////                points[i] = new Point2D.Double(scanner.nextDouble(), scanner.nextDouble());
//
//                points.add(new Point(scanner.nextDouble(), scanner.nextDouble()));
//            }
//            Pair pair = divideAndConquer(points);
//
//            System.out.println(pair);
//            scanner.close();
//        }
    }
}