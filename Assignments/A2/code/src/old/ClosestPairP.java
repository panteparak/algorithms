package old;

import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

public class ClosestPairP {

    static class FastScanner {
        private BufferedReader reader;
        private StringTokenizer tokenizer;

        public FastScanner(InputStream in) {
            this.reader = new BufferedReader(new InputStreamReader(in));
        }

        private String next(){
            if (tokenizer == null || !tokenizer.hasMoreElements()) try {
                tokenizer = new StringTokenizer(reader.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return tokenizer.nextToken();
        }

        public String nextString() {
            return next();
        }

        public double nextDouble(){
            return Double.parseDouble(next());
        }

        public int nextInt(){
            return Integer.parseInt(next());
        }

        public long nextLong(){
            return Long.parseLong(next());
        }
    }

    static class Pair<T extends Point2D> {
        private T pairOne;
        private T pairTwo;
        private double distance = 0.0;

        public Pair(T pairOne, T pairTwo) {
            this.pairOne = pairOne;
            this.pairTwo = pairTwo;
            this.distance = pairOne.distance(pairTwo);
        }

        public T getPairOne() {
            return pairOne;
        }

        public T getPairTwo() {
            return pairTwo;
        }

        public double getDistance() {
            return distance;
        }

        public Pair<T> compareLessDistance(Pair<T> that){
            return this.distance > that.getDistance() ? that : this;
        }

        @Override
        public String toString() {
            return pairOne.getX() + " " + pairOne.getY() + " " + pairTwo.getX() + " " + pairTwo.getY();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Pair<?> pair = (Pair<?>) o;

            if (Double.compare(pair.distance, distance) != 0) return false;
            if (pairOne != null ? !pairOne.equals(pair.pairOne) : pair.pairOne != null) return false;
            return pairTwo != null ? pairTwo.equals(pair.pairTwo) : pair.pairTwo == null;
        }

        @Override
        public int hashCode() {
            int result;
            long temp;
            result = pairOne != null ? pairOne.hashCode() : 0;
            result = 31 * result + (pairTwo != null ? pairTwo.hashCode() : 0);
            temp = Double.doubleToLongBits(distance);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            return result;
        }
    }

    public static void main(String[] args) {
//        FastScanner scanner = new FastScanner(System.in);
//
//        int n;
//        while ((n = scanner.nextInt()) != 0){
//            Point2D[] points = new Point2D[n];
//
//            for (int i = 0; i < n; i++){
//                points[i] = new Point2D.Double(scanner.nextDouble(), scanner.nextDouble());
//            }
//            System.out.println(solve(points));
//
//        }

        Point2D[] points = new Point2D[5];
        points[0] = new Point2D.Double(5, 4);
        points[1] = new Point2D.Double(12, 21);
        points[2] = new Point2D.Double(19, 20);
        points[3] = new Point2D.Double(20, 20);
        points[4] = new Point2D.Double(9, 20);


//        Pair<Point2D> p = solve(points);
//        Pair<Point2D> q = findClosest(points);
//
//        System.out.println(p);
//        System.out.println(q);
//        System.out.println(p.equals(q));

    }

    private static Pair<Point2D> solve(Point2D[] points){
        Point2D[] x = Arrays.copyOf(points, points.length);
        Point2D[] y = Arrays.copyOf(points, points.length);
        Arrays.sort(x, Comparator.comparingDouble(Point2D::getX));
        Arrays.sort(y, Comparator.comparingDouble(Point2D::getY));
        return closestPair(x, y, 0, x.length);
    }

    private static Pair<Point2D> closestPair(Point2D[] sortedX, Point2D[] sortedY, int lo, int hi){
        if (hi - lo + 1 < 4)
            return closestPair(sortedX, lo, hi);

        int mid = (lo + hi)/2;
        Pair<Point2D> left = closestPair(sortedX, sortedY, lo, mid);
        Pair<Point2D> right = closestPair(sortedX, sortedY, mid, hi);

        Pair<Point2D> delta = left.compareLessDistance(right);

        List<Point2D> lst = new ArrayList<>(hi-lo + 1);

        Point2D[] deltaBand = new Point2D[hi - lo +1];
        for (int i = lo; i <= hi; i++) {
            if (Math.abs(sortedY[i].getX() - sortedX[mid].getX()) < delta.distance)
                deltaBand[i] = sortedY[i];
        }

//        for (int i = 0; )



        return delta;
    }

    private static Pair<Point2D> closestPair(Point2D[] points, int lo, int hi){

        if (hi - lo + 1 < 3){
            return new Pair<>(points[lo], points[hi]);
        }

        Pair<Point2D> possible = null;
        for (int i = lo; i < hi - 1; i++){
            for (int j = i + 1; j < hi; j++){
                Point2D pointA = points[i];
                Point2D pointB = points[j];
                if (possible == null || possible.distance > pointA.distance(pointB))
                    possible = new Pair<>(pointA, pointB);
            }
        }
        return possible;
    }

}
