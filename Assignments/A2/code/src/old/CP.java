package old;

import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Comparator;
import java.util.StringTokenizer;

public class CP {

    static class FastScanner {
        private BufferedReader reader;
        private StringTokenizer tokenizer;

        public FastScanner(InputStream in) {
            this.reader = new BufferedReader(new InputStreamReader(in));
        }

        private String next(){
            if (tokenizer == null || !tokenizer.hasMoreElements()) try {
                tokenizer = new StringTokenizer(reader.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return tokenizer.nextToken();
        }

        public String nextString() {
            return next();
        }

        public double nextDouble(){
            return Double.parseDouble(next());
        }

        public int nextInt(){
            return Integer.parseInt(next());
        }

        public long nextLong(){
            return Long.parseLong(next());
        }


    }

    public static void main(String[] args) {
        FastScanner scanner = new FastScanner(System.in);
        int n;
        while ((n = scanner.nextInt()) != 0){

            Point2D[] pointsX = new Point2D[n];
            Point2D[] pointsY = new Point2D[n];

            for (int i = 0; i < n; i++) {
                double x = scanner.nextDouble();
                double y = scanner.nextDouble();

                pointsX[i] = new Point2D.Double(x, y);
                pointsY[i] = new Point2D.Double(x, y);
            }

            if (n == 1){
                System.out.println("INFINITY");
            } else {
                double result = solve(pointsX, pointsY);

                if (result > 10000){
                    System.out.println("INFINITY");
                }else {
                    System.out.println(String.format("%.4f", result));
                }
            }
        }
    }

    private static double solve(Point2D[] pointX, Point2D[] pointY){
        Arrays.sort(pointX, Comparator.comparingDouble(Point2D::getX));
        Arrays.sort(pointY, Comparator.comparingDouble(Point2D::getY));

        return closestPair(pointX, pointY);
    }

    private static double closestPair(Point2D[] point){
        if (point.length == 2){
            return point[0].distance(point[1]);
        }else {
            double min = Double.MAX_VALUE;
            for (int i = 0; i < point.length - 1; i++){
                for (int j = 0; j < point.length; j++){
                    if (min > point[i].distance(point[j]))
                        min = point[i].distance(point[j]);
                }
            }
            return min;
        }
    }

    private static double closestPair(Point2D[] pointX, Point2D[] pointY){
        if (pointX.length <= 3)
            return closestPair(pointX);

//        int mid = pointX.length -  (pointX.length / 2);
        int mid = pointX.length/2;
        double left = closestPair(Arrays.copyOfRange(pointX, 0, mid), pointY);
        double right = closestPair(Arrays.copyOfRange(pointX, mid, pointX.length), pointY);

        System.out.println(  "left: "+ left);
        System.out.println("right: " + right);
        double min = Double.min(left, right);
//        System.out.println(min);


//        for (int i = 0; i < pointY.length - 1; i++){
//            for (int j = i + 1; i < pointY.length && min > pointX[i].distance(pointY[j]) ; j++){
//                min = pointX[i].distance(pointY[j]);
//            }
//        }
        return min;
    }
}
