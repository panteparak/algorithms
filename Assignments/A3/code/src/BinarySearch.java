public class BinarySearch {

    public static int search(int[] A){
        return search(A, 0, A.length);
    }

    public static int search(int[] A, int lo, int hi){

        if (lo >= hi) return -1; // if not exist

        int mid = (lo + hi) / 2;
        if (A[mid] == mid) return mid;
        else if (A[mid] > mid) return search(A, lo, mid);
        else return search(A, mid + 1, hi);
    }
}
