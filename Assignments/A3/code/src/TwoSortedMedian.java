public class TwoSortedMedian {
    public static double findMedian(int[] A, int[] B){
        return findMedian(A, 0, A.length, B, 0, B.length);
    }

    public static double findMedian(int[] A, int loA, int hiA, int[] B,int loB, int hiB){
        if (sizeOf(loA, hiA) == 1 && sizeOf(loB, hiB) == 1)
            return (A[loA] + B[loB])/ 2.0;

        if (sizeOf(loA, hiA) == 2 && sizeOf(loB, hiB) == 2)
            return (Math.max(A[loA], B[loB]) + Math.min(A[hiA], B[hiB]))/2.0;

        int mid1 = median(loA,hiA);
        int mid2 = median(loB, hiB);

        if (A[mid1] == B[mid2])
            return A[mid1];
        else if (A[mid1] > B[mid2])
            return findMedian(A, loA, mid1, B, mid2, hiB-1);
        else
            return findMedian(A, mid1, hiA-1, B, loB, mid2);
    }

    private static int sizeOf(int lo, int hi){
        return hi - lo;
    }

    private static int median(int lo, int hi) {
        return (lo + hi) / 2;
    }
}
