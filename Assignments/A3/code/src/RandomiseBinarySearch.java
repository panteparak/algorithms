import java.util.concurrent.ThreadLocalRandom;

public class RandomiseBinarySearch {
    public static boolean search(int[] array, int x){
        return search(array, 0, array.length, x);
    }

    private static boolean search(int[] array, int lo, int hi, int x) {
        if (lo >= hi) return false;
        else {
            int mid = ThreadLocalRandom.current().nextInt(lo, hi);

            if (array[mid] == x) return true;
            else if (array[mid] > x) return search(array, lo, mid-1, x);
            else return search(array, mid+1, hi, x);
        }
    }

    public static void main(String[] args) {
        int[] arr = {1, 9, 27, 32, 109, 127};
        System.out.println(search(arr, 27));
    }
}
