import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

public class CD {
    static class FastScanner {
        private BufferedReader reader;
        private StringTokenizer token;

        public FastScanner(InputStream in) {
            reader = new BufferedReader(new InputStreamReader(in));
        }

        private String next(){
            if (token == null || !token.hasMoreElements())try {
                    token = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            return token.nextToken();
        }

        public int nextInt(){
            return Integer.parseInt(next());
        }
    }

    public static void main(String[] args) {
        FastScanner fastScanner = new FastScanner(System.in);

        int n;
        int m;
        while ((n = fastScanner.nextInt()) != 0 && (m = fastScanner.nextInt()) != 0){
            Set<Integer> setA = new HashSet<>(n);
            Set<Integer> setB = new HashSet<>(m);

            for (int i = 0; i < n; i++){
                setA.add(fastScanner.nextInt());
            }
            for (int i = 0; i < m; i++){
                setB.add(fastScanner.nextInt());
            }
            setA.retainAll(setB);
            System.out.println(setA.size());
        }
    }
}
