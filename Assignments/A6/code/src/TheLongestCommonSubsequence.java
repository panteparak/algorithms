import java.math.*;
import java.util.*;
import java.io.*;

public class TheLongestCommonSubsequence {
    static class FastScanner {
        private BufferedReader reader;
        private StringTokenizer token;

        public FastScanner(InputStream in) {
            reader = new BufferedReader(new InputStreamReader(in));
        }

        private String next() {
            while (token == null || !token.hasMoreTokens() ) {
                try {
                    token = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                }
            }
            return token.nextToken();
        }

        public String nextString(){
            return next();
        }

        public Integer nextInt(){
            return Integer.parseInt(next());
        }

        public Double nextDouble(){
            return Double.parseDouble(next());
        }

        public Long nextLong(){
            return Long.parseLong(next());
        }

        public BigInteger nextBigInteger(){
            return new BigInteger(next());
        }
    }

    public static void main(String[] args) {
        FastScanner fs = new FastScanner(System.in);

        int n = fs.nextInt(), m = fs.nextInt();

        int[] A = new int[n];
        int[] B = new int[m];
        for (int i = 0; i < n; i++){
            A[i] = fs.nextInt();
        }

        for (int i = 0; i < m; i++){
            B[i] = fs.nextInt();
        }
    }

    private static List<Integer> solve(int[] A, int[] B){
        
    }
}
