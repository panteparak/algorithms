import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

public class Bicoloring {
    static class FastScanner {
        private BufferedReader reader;
        private StringTokenizer token;

        public FastScanner(InputStream in) {
            reader = new BufferedReader(new InputStreamReader(in));
        }

        private String next() {
            if (token == null || !token.hasMoreElements()){
                try {
                    token = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    return null;
                }
            }
            return token.nextToken();
        }

        private boolean hasNext(){
            return token == null || token.hasMoreElements();
        }

        public String nextString(){
            return next();
        }

        public Integer nextInt(){
            return Integer.parseInt(next());
        }

        public Double nextDouble(){
            return Double.parseDouble(next());
        }

        public Long nextLong(){
            return Long.parseLong(next());
        }
    }


    public static void main(String[] args) {
        FastScanner fs = new FastScanner(System.in);

        int n;
        while ((n = fs.nextInt()) != 0){
            int edges = fs.nextInt();
            Map<Integer, Set<Integer>> graphs = new HashMap<>(edges);

            for (int i = 0; i < edges; i++){
                int u = fs.nextInt();
                int v = fs.nextInt();

                if (!graphs.containsKey(u)) graphs.put(u, new HashSet<>(edges));
                graphs.get(u).add(v);

                if (!graphs.containsKey(v)) graphs.put(v, new HashSet<>(edges));
                graphs.get(v).add(u);
            }

            Set<Integer> visited = new HashSet<>();


//            System.out.println(graphs);
            boolean result = solve(graphs, visited, 0);
            if (result){
                System.out.println("BICOLORABLE.");
            }else {
                System.out.println("NOT BICOLORABLE.");
            }

        }
    }

    private static boolean solve(Map<Integer, Set<Integer>> graphs, Set<Integer> visited, Integer start){
        Queue<Integer> queue = new ArrayDeque<>();
        queue.add(start);
        Map<Integer, Integer> coloring = new HashMap<>();


        while (!queue.isEmpty()){
            Integer node = queue.poll();

            if (!visited.contains(node)){
                visited.add(node);
                if (graphs.containsKey(node)){

                    if (!coloring.containsKey(node))
                        coloring.put(node, 1);


                    Integer current = coloring.get(node);
                    Integer opposite = current == 1 ? 2 : 1;

                    for (Integer n : graphs.get(node)){
                        if (visited.contains(n)) continue;

                        if (!coloring.containsKey(n)){
                            coloring.put(n, opposite);
                            queue.add(n);
                        }else if (coloring.get(n).equals(current)) {
                            return false;

                        }
                    }
                }
            }
        }

        return true;
    }
}
