import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

public class ComeAndGo {
    static class Graph<T extends Comparable<T>> {
        private Map<T, Set<T>> graph;
        private Set<T> uniqueNodes;
        private Integer edges;

        public Graph() {
            this.graph = new HashMap<>();
            this.edges = 0;
            this.uniqueNodes = new HashSet<>();
        }

        public void addNode(T u){
            uniqueNodes.add(u);
            if (! graph.containsKey(u)) graph.put(u, new HashSet<>());

        }

        public void addNode(T u, T v){
            addNode(u);
            graph.get(u).add(v);
            this.edges++;
        }

        public void addNode(T u, T v, int w){
            if (w <= 2) addNode(u, v);
            if (w == 2) addNode(v, u);
        }

        public Set<T> getAllVertex(){
            return uniqueNodes;
        }

        public boolean containVertex(T u){
            return graph.containsKey(u);
        }

        public Set<T> vertexOf(T u){
            return graph.get(u);
        }

        public Integer size(){
            return uniqueNodes.size();
        }

        public Integer edges(){
            return edges;
        }


        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("Graph {").append("\n");
            Iterator<T> itr = graph.keySet().iterator();
            while (itr.hasNext()) {
                T next = itr.next();
                builder.append("\t").append(next).append(" -> ").append(this.graph.get(next)).append("\n");
            }

            builder.append("}");

            return builder.toString();
        }
    }
    static class  FastScanner {
        private BufferedReader reader;
        private StringTokenizer token;

        public FastScanner(InputStream in) {
            reader = new BufferedReader(new InputStreamReader(in));
        }

        private String next() {
            if (token == null || !token.hasMoreElements()){
                try {
                    token = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    return null;
                }
            }
            return token.nextToken();
        }

        private boolean hasNext(){
            return token == null || token.hasMoreElements();
        }

        public String nextString(){
            return next();
        }

        public Integer nextInt(){
            return Integer.parseInt(next());
        }

        public Double nextDouble(){
            return Double.parseDouble(next());
        }

        public Long nextLong(){
            return Long.parseLong(next());
        }
    }

    public static void main(String[] args) {
        FastScanner fs = new FastScanner(System.in);

        int n, m;
        while ((n = fs.nextInt()) != 0 && (m = fs.nextInt()) != 0){
            Graph<Integer> graph = new Graph<>();
            Graph<Integer> reverseGraph = new Graph<>();
            for (int i = 0; i < m; i++){
                int u = fs.nextInt();
                int v = fs.nextInt();
                int w = fs.nextInt();
                graph.addNode(u, v, w);
                reverseGraph.addNode(v, u, w);
            }

            if (solve(graph, reverseGraph)) System.out.println("1");
            else System.out.println("0");

        }
    }

    private static boolean solve(Graph<Integer> graph, Graph<Integer> reverse){
        Set<Integer> visited = new HashSet<>();
        walk(graph, visited, 1);

        Set<Integer> visitedReverse = new HashSet<>();
        walk(reverse, visitedReverse, 1);
        return visited.containsAll(visitedReverse) && visited.containsAll(graph.uniqueNodes) && visitedReverse.containsAll(reverse.uniqueNodes);
    }

    private static void walk(Graph<Integer> graph, Set<Integer> visited, Integer start){
        Stack<Integer> stack = new Stack<>();
        stack.add(start);

        while (!stack.isEmpty()){
            Integer node = stack.pop();

            if (!visited.contains(node)){

                visited.add(node);
                if (graph.containVertex(node))
                    stack.addAll(graph.vertexOf(node));
            }
        }
    }
}
