package OldTree;

import java.util.*;

public class IsTree {
    static class Graph<T extends Comparable<T>> {
        private Map<T, Set<T>> graph;
        private Set<T> uniqueNodes;
        private Integer edges;

        public Graph() {
            this.graph = new HashMap<>();
            this.edges = 0;
            this.uniqueNodes = new HashSet<>();
        }

        public void addNode(T u){
            uniqueNodes.add(u);
            if (! graph.containsKey(u)) graph.put(u, new HashSet<>());

        }

        public void addNode(T u, T v){
            addNode(u);
            graph.get(u).add(v);
            this.edges++;
        }

        public Set<T> getAllVertex(){
            return uniqueNodes;
        }

        public boolean containVertex(T u){
            return graph.containsKey(u);
        }

        public Set<T> vertexOf(T u){
            return graph.get(u);
        }

        public Integer size(){
            return uniqueNodes.size();
        }

        public Integer edges(){
            return edges;
        }


        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("Graph {").append("\n");
            Iterator<T> itr = graph.keySet().iterator();
            while (itr.hasNext()) {
                T next = itr.next();
                builder.append("\t").append(next).append(" -> ").append(this.graph.get(next)).append("\n");
            }

            builder.append("}");

            return builder.toString();
        }
    }

    private boolean walk(Graph<Integer> graph, Set<Integer> visited, int start){
//        Stack<Integer> stack = new Stack<>();
//        stack.add(start);
//
//        while (!stack.isEmpty()){
//            int node = stack.pop();
//
//            if (!visited.contains(node)){
//                visited.add(node);
//                if (graph.containVertex(node))
//                    stack.addAll(graph.vertexOf(node));
//            } else
//                return true;
//        }
        return false;

//
//        visited.add(start);
//
//        if ()
    }

    public boolean isATree(Graph<Integer> graph){
        Set<Integer> visited = new HashSet<>();
        return walk(graph, visited, graph.uniqueNodes.iterator().next());
    }

    public static void main(String[] args) {
        Graph<Integer> graph = new Graph<>();
        graph.addNode(1, 0);
        graph.addNode(0, 1);

        graph.addNode(0, 2);
        graph.addNode(2, 0);

        graph.addNode(0, 3);
        graph.addNode(3, 0);
        graph.addNode(3, 4);
        graph.addNode(4, 3);

        Graph<Integer> g = new Graph<>();
        g.addNode(1, 0);
        g.addNode(0, 1);

        g.addNode(0, 2);
        g.addNode(2, 0);
        g.addNode(2, 1);
        g.addNode(1, 2);
        g.addNode(0, 3);
        g.addNode(3, 0);
        g.addNode(3, 4);
        g.addNode(4, 3);

        IsTree tree = new IsTree();
        System.out.println(tree.isATree(graph));
        System.out.println(tree.isATree(g));
    }
}
