import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Sep {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);

        int n, m, c = 0;
        while ((n = scanner.nextInt()) != 0 && (m = scanner.nextInt()) != 0){
            int[] arr = new int[3 * m];
            c++;
            for (int i = 0; i < m; i+= 3){
                int u = scanner.nextInt();
                int v = scanner.nextInt();
                int w = scanner.nextInt();

                arr[i] = u;
                arr[i+ 1] = v;
                arr[i + 2] = w;
            }

            write(arr, n ,m, c);
        }
    }

    private static void write(int[] array, int n ,int m, int c) throws IOException {
        File file = new File("/Users/panteparak/Desktop/out/" + c);
        FileWriter writer = new FileWriter(file);

        if (!file.exists())
            file.createNewFile();

        writer.write(String.format("%d %d\n", n, m));

        for (int i = 0; i < array.length; i+= 3){
            writer.write(String.format("%d %d %d\n", array[i], array[i+1], array[i+2]));
        }

        writer.write("0 0\n");
        writer.flush();
    }
}
