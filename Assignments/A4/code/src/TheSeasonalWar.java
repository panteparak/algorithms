import java.io.*;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.concurrent.atomic.AtomicInteger;

public class TheSeasonalWar {
    static int size;
    static String[][] map;
    static boolean[][] visited;

    static class FastScanner {
        private BufferedReader reader;
        private StringTokenizer token;

        public FastScanner(InputStream in) {
            reader = new BufferedReader(new InputStreamReader(in));
        }

        private String next() {

            String out = null;
            try {
                String st;
                if ((token == null || !token.hasMoreElements())){
                    if ((st = reader.readLine()) != null){
                        token = new StringTokenizer(st);
                    }
                    out = token.nextToken();
                }
            } catch (IOException | NoSuchElementException e){}

            return out;
        }

        public String nextString(){
            return next();
        }

        public Integer nextInt(){
            return Integer.parseInt(next());
        }

        public Double nextDouble(){
            return Double.parseDouble(next());
        }

        public Long nextLong(){
            return Long.parseLong(next());
        }
    }

    public static void main(String[] args) {
        FastScanner fs = new FastScanner(System.in);
        String n;
        int r = 0;
        while ((n = fs.nextString()) != null){
            size = Integer.parseInt(n);

            map = new String[size][size];
            visited = new boolean[size][size];

            for (int i = 0; i < size; i++){
                map[i] = fs.nextString().split("");
            }

            System.out.println(String.format("Image number %d contains %d war eagles.", ++r, solve()));

        }
    }

    public static int solve(){
        int count = 0;
        for (int i = 0; i < map.length; i++){
            for (int j = 0; j < map[i].length; j++){
                AtomicInteger n = new AtomicInteger(0);
                walk(i, j, n);

                if (n.get() > 0)
                    count++;
            }
        }

        return count;
    }

    public static void walk(int r, int c, AtomicInteger war){
        if (validMove(r, c)){
            visited[r][c] = true;
            war.incrementAndGet();

            walk(r + 1, c, war);
            walk(r - 1, c, war);
            walk(r, c + 1, war);
            walk(r, c - 1, war);

            walk(r + 1, c + 1, war);
            walk(r - 1, c - 1, war);
            walk(r - 1, c + 1, war);
            walk(r + 1, c - 1, war);
        }
    }

    private static boolean validMove(int r, int c){
        return (0 <= r && 0 <= c) && (r < size && c < size) && !visited[r][c] && map[r][c].equals("1");
    }
}
